Rails.application.routes.draw do

  resources :posts do
    resources :comments
    resources :likes
  end


  devise_for :users,  controllers: { sessions: 'users/sessions' }

  resources :users do
    member do
      get :add_friend
      get :un_friend 
      get :accept
      get :cancel
      get :friend
    end
  end
  
  get 'welcome/index'

  root 'welcome#index'

   # resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
