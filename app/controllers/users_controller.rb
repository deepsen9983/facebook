class UsersController < ApplicationController
  def index
    @users = User.all.where.not(id: current_user.id).page(params[:page]).per(10)
  end 

  def show
    @user = User.find_by_id(params[:id])
  end

  def add_friend
    @user = User.find(params[:id])
    #current_user == @user
    current_user.follow(@user)
    @follow = Follow.find_by(follower: current_user, followable: @user)
    respond_to do |format|
       format.js 
    end
  end

  def un_friend
    @user = User.find(params[:id])
    current_user.stop_following(@user) || @user.stop_following(current_user) 
     respond_to do |format|
       format.js 
      end
  end

  
  def accept
    puts params
    @user = User.find(params['user']) 
    @current_user = User.find(params[:id]) 
    @follow = Follow.find_by(follower_id: @user.id, followable_id: @current_user.id)
    @follow.update(isaccepted: true)
     respond_to do |format|
       format.js 
     end
  end

   def cancel
    @user = User.find(params['user']) 
    @current_user = User.find(params[:id])
     #@current_user.stop_following(@user)
    @follow = Follow.find_by(follower_id: @user.id, followable_id: @current_user.id)
    @follow.destroy
      respond_to do |format|
       format.js 
      end
   end

   #def friend
    #@friends = Follow.where(followable_id: current_user, isaccepted: true)
    # puts '*********'

     #puts @friends.inspect
     #puts '@@@@@@@@@@'
     #render 'friend'
   #end

  
end
