class CommentsController < ApplicationController
 

  def index
  end


  def new
    @post = Post.find(params[:post_id])
    @comment = Comment.new  
  end

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to post_path(@post.id), notice: 'Comments was successfully created.' }
        format.json { render :form, status: :created, location:@comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end 
  end

  def show
  end

    private

       def comment_params
         params.require(:comment).permit(:body, :user_id)
       end
end



