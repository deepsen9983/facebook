class User < ApplicationRecord
  has_many :posts
  has_many :comments
  has_many :likes, dependent: :destroy

  acts_as_followable
  acts_as_follower

   paginates_per 5
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,:confirmable

    def username
       return self.email.split('@')[0].capitalize
    end 

    def is_accepted?(current_user, user)
      Follow.where(follower_id: current_user.id, followable_id: user.id).first.try(:isaccepted)
      # puts '@@@@@@@@@@@@@@@@'
      # puts current_user.inspect
      # puts user.inspect
      # puts '@@@@@@@@@@@@@@@@'
      # # @user = User.find(params[:id])
      # # puts self.follow
      # @accept = Follow.where(follower_id: current_user.id, followable_id: @user.id, isaccepted: true)
      # return @accept
    end

    def get_friendship_status(user)
      relationship = Follow.find_by(follower_id: self.id, followable_id: user.id) || Follow.find_by(follower_id: user.id, followable_id: self.id)
      if relationship
        if relationship.isaccepted
          return "friend"
        else
          return "pending"
        end
      else
        return "not yet"
      end
    end
   
end
