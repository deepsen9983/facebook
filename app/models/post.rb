class Post < ApplicationRecord
  belongs_to :user
  mount_uploader :picture, PictureUploader
  has_many :comments
  has_many :likes, dependent: :destroy
  validates :content, presence: true, length: { maximum: 140 }
end
