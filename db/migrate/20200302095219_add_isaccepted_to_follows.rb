class AddIsacceptedToFollows < ActiveRecord::Migration[6.0]
  def change
    add_column :follows, :isaccepted, :boolean,  default: false
  end
end
